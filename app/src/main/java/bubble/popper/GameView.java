package bubble.popper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

/**
 * Luokka pelilogiikalle, piirtämiselle, äänille
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    /** Nuolen paksuus */
    public static final double POINTER_WIDTH = 1.0 / 125;
    /** Nuolen pituus */
    public static final double POINTER_LENGTH = 1.0 / 4;
    /** Nuolen kärjen paksuus */
    public static final double ARROW_LENGHT = 1.0 / 10;
    /** Kuplien säde */
    public static final double BUBBLE_RADIUS = 1.0 / 40;
    /** Kuplien ampumisnopeus */
    public static final double BUBBLE_SPEED = 2.0 / 2;
    /** Tekstien koko */
    public static final double TEXT_SIZE = 1.0 / 20;

    /** Äänien toistaja */
    private SoundPool soundPool;
    /** Äänien lista */
    private SparseIntArray soundMap;
    /** Ampujan äänitehosteen id */
    public static final int SOUND_SHOOTER = 0;
    /** Törmäysäänitehosteen id */
    public static final int SOUND_IMPACT = 1;
    /** Poksahdusäänitehosteen id */
    public static final int SOUND_POP = 2;
    /** Voittoäänitehosteen id */
    public static final int SOUND_VICTORY = 3;
    /** Häviöäänitehosteen id */
    public static final int SOUND_LOSE = 4;

    /** Ruudun leveys */
    private int screenWidth;
    /** Ruudun pituus */
    private int screenHeight;
    /** Kuplan halkaisija */
    public int width;
    /** Tila kuplien välissä */
    public int spacing;

    /** Kangas johon piirretään*/
    private Canvas canvas = null;
    /** Tekstien maali */
    private Paint textPaint;
    /** Taustavärin maali */
    private Paint backgroundPaint;
    /** Musta maali */
    private Paint buttonPaint;
    /** Seuraavan kuplan värin maali */
    private Paint nextPaint;
    /** Arvottuun väriteemaan sopivan tumman sävyn maali */
    private Paint darkPaint;
    /** Pelin alarajan maali */
    private Paint linePaint;

    /** Kuplaruudukon rivien määrä */
    public static final int rows = 13;
    /** Kuplaruudukon sarakkeiden määrä */
    public static final int columns = 10;
    /** Kuinka monta riviä on täytetty pelin alussa */
    public static final int filledrows = 6;
    /** Kaksiulotteinen taulukko kuplaruudukolle */
    private StaticBubble[][] grid;
    /** Lista kuplien väreistä */
    private ArrayList<Integer> bubbleColors;
    /** Lista taustaväreistä */
    private ArrayList<String> bgColors;
    /** Kuplien ampuja */
    private Shooter shooter;
    /** Ammuttava kupla */
    private Bubble currentBubble;
    /** Seuraava ammuttava kupla */
    private Bubble nextBubble;
    /** Pelaajan pistemäärä */
    private int score;
    /** Ammuttavan kuplan väri */
    public int currentColor;
    /** Seuraavan ammuttavan kuplan väri */
    public int nextColor;
    /** Ampumiskertojen määrä */
    private int shotCount;
    /** Pelimuodon muuttuja (0/1) */
    public int gameMode = 0;
    /** Vaikeustason muuttuja (0/1/2) */
    public int difficulty = 0;
    /** Kuinka monta kertaa pelin kattoa on madallettu */
    private int gridMoves;
    /** Pelin katon y-koordinaatti */
    private int gridY;
    /** Totuusarvo sille, saako pelaaja ampua */
    private boolean allowShoot = true;
    /** Satunnaisten arvojen arpomiseen */
    Random random = new Random();

    /** Pelin säie */
    private GameThread gameThread;
    /** Totuusarvo sille, onko dialogi näkyvillä */
    private boolean dialogIsDisplayed = false;
    private Activity activity;
    private static String helpMessageId;
    private static int helpScore;
    private static GameView helpGameView;
    private static boolean helpDialogIsDisplayed;

    /** Alustaja. Alustetaan mm. äänet, piirtämisasetukset ja kuplien värit.
     * @param context
     * @param attrs
     */
    public GameView(Context context, AttributeSet attrs) {

        super(context, attrs);
        activity = (Activity) context;
        getHolder().addCallback(this);

        AudioAttributes.Builder attrBuilder = new AudioAttributes.Builder();
        attrBuilder.setUsage(AudioAttributes.USAGE_GAME);
        SoundPool.Builder builder = new SoundPool.Builder();
        builder.setMaxStreams(32);
        builder.setAudioAttributes(attrBuilder.build());
        soundPool = builder.build();
        soundMap = new SparseIntArray(32);
        soundMap.put(SOUND_SHOOTER, soundPool.load(context, R.raw.shooter, 1));
        soundMap.put(SOUND_IMPACT, soundPool.load(context, R.raw.impact, 1));
        soundMap.put(SOUND_POP, soundPool.load(context, R.raw.pop, 1));
        soundMap.put(SOUND_VICTORY, soundPool.load(context, R.raw.victory, 1));
        soundMap.put(SOUND_LOSE, soundPool.load(context, R.raw.lose, 1));
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        textPaint = new Paint();
        nextPaint = new Paint();
        darkPaint = new Paint();
        linePaint = new Paint();
        buttonPaint = new Paint();
        backgroundPaint = new Paint();
        buttonPaint.setColor(Color.BLACK);
        darkPaint.setStrokeWidth(30);
        linePaint.setStrokeWidth(10);

        bubbleColors = new ArrayList<>();
        bubbleColors.add(Color.BLUE);
        bubbleColors.add(Color.RED);
        bubbleColors.add(Color.GREEN);
        bubbleColors.add(Color.YELLOW);
        bubbleColors.add(Color.MAGENTA);
        bubbleColors.add(Color.CYAN);

        bgColors = new ArrayList<>();
        bgColors.add("#E8FFFE");
        bgColors.add("#FFA67C");
        bgColors.add("#79FFC4");
        bgColors.add("#FFFFC0");
        bgColors.add("#E8C5FF");
        bgColors.add("#FFABE0");
    }

    /** Aloittaa uuden pelin. Resetoidaan tarvittavat muuttujat ja luodaan uusi ruudukko kuplia. */
    public void startGame(){

        gridY = 0;
        gridMoves = 0;
        shotCount = 0;
        score = 0;

        int tempcolor = backgroundPaint.getColor();
        while (tempcolor == backgroundPaint.getColor()){
            tempcolor = Color.parseColor(bgColors.get(random.nextInt(bgColors.size())));
        }
        backgroundPaint.setColor(tempcolor);
        switch (tempcolor){
            case -1507330:
                darkPaint.setColor(Color.parseColor("#3796A5"));
                break;
            case -64:
                darkPaint.setColor(Color.parseColor("#FF9A00"));
                break;
            case -21536:
                darkPaint.setColor(Color.parseColor("#9C1F66"));
                break;
            case -22916:
                darkPaint.setColor(Color.parseColor("#8D4623"));
                break;
            case -1522177:
                darkPaint.setColor(Color.parseColor("#681984"));
                break;
            case -8781884:
                darkPaint.setColor(Color.parseColor("#2E866E"));
                break;
        }

        shooter = new Shooter(this, (int) (POINTER_LENGTH * screenWidth), (int) (POINTER_WIDTH * screenHeight), (int) (ARROW_LENGHT * screenWidth));
        grid = new StaticBubble[rows][columns];
        width = screenWidth / (columns+1);
        spacing = width / 10;
        int bubbleX = 0;
        int bubbleY = 0 + spacing / 2;
        boolean even = true;

        for (int i = 0; i < rows; i++){
            if (even == true){
                bubbleX = 0 + spacing;
                even = false;
            }
            else if (even == false){
                bubbleX = width / 2 + spacing;
                even = true;
            }
            for (int j = 0; j < columns; j++){
                if (i < filledrows){
                    int color = bubbleColors.get(random.nextInt(bubbleColors.size()));
                    if (even == true && j == columns-1){
                        grid[i][j] = new StaticBubble(this, 2, i, j, 0, bubbleX, bubbleY, width, even);
                    }
                    else {
                        grid[i][j] = new StaticBubble(this, 1, i, j, color, bubbleX, bubbleY, width, even);
                    }
                }
                else {
                    if (even == true && j == columns-1){
                        grid[i][j] = new StaticBubble(this, 2, i, j, 0, bubbleX, bubbleY, width, even);
                    }
                    else {
                        grid[i][j] = new StaticBubble(this, 0, i, j, Color.GRAY, bubbleX, bubbleY, width, even);
                    }
                }
                bubbleX += width + spacing;
            }
            bubbleY += width + (spacing / 2);
        }

        pickNextColor();
        pickCurrentColor();
        pickNextColor();
        currentBubble = new Bubble(this, currentColor, screenWidth/2 - width/2, screenHeight - width, width);
        nextBubble = new Bubble(this, nextColor, (int)(getScreenWidth() / 1.25), (int)(getScreenHeight() / 1.15), width);
        allowShoot = true;
        gameThread = new GameThread(getHolder());
        gameThread.start();
    }

    /** Lasketaan ampujalle oikea kulma kosketuspisteen perusteella
     * @param event Kosketus
     */
    public void alignShooter(MotionEvent event) {

        Point touchPoint = new Point((int) event.getX(), (int) event.getY());

        double centerMinusX = (screenWidth/ 2 - touchPoint.x);
        double centerMinusY = (screenHeight / 2 - (touchPoint.y / 2));

        double angle = Math.atan2(centerMinusY, centerMinusX);
        shooter.align(angle);
    }

    /** Ammutaan uusi kupla jos ehdot täyttyvät.
     * @param event Kosketus
     */
    public void shootBubble(MotionEvent event){

        if (allowShoot == true){
            playSound(SOUND_SHOOTER);
            allowShoot = false;
            shooter.shoot(width, currentColor);
            shotCount++;
            pickCurrentColor();
        }
    }

    /** Tutkitaan, mihin ammuttu kuula osuu ja milloin. Kun osuma tulee, kutsutaan lockToGrid-funktiota. */
    public void bubbleCollision(){

        if (shooter.getShotBubble() != null && shooter.getShotBubble().isOnScreen()){
            int color = shooter.getShotBubble().getColor();
            for (int i = 0; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    StaticBubble temp = grid[i][j];
                    try {
                        if (shooter.getShotBubble().collides(temp) || shooter.getShotBubble().outlineRect.top < gridY) {
                            Point collisionPoint = new Point(shooter.getShotBubble().currentX(), shooter.getShotBubble().currentY());
                            shooter.removeShotBubble();
                            playSound(SOUND_IMPACT);
                            j--;
                            lockToGrid(collisionPoint, color);
                            break;
                        }
                    }catch (Exception e){
                    }
                }
            }
        }
    }

    /** Kuplan suunnan ennustajan piirtämiseen käytettävä apufunktio. Ilmaisee, milloin avustajaa ei enää tarvitse piirtää. */
    public boolean helperCollision(RectF tempRect){

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                StaticBubble temp = grid[i][j];
                try {
                    if (temp.getType() == 1 && RectF.intersects(temp.outlineRect, tempRect)){
                        return true;
                    }
                }
                catch (Exception e){
                }
            }
        }
        return false;
    }

    /** Luodaan uusi StaticBubble lähimpään vapaaseen ruudukon paikkaan.
     * Jos ei tule Game Overia, tutkitaan syntyikö vähintään kolmen kuplan ketjua ja poistetaan ne pelistä tarvittaessa.
     * Samalla kutsutaan checkForIsolated-funktiota joka tutkii, syntyikö erillisiä kuplaryhmiä.
     * Lopuksi tarkastetaan, voittiko pelaaja.
     * @param collisionPoint Kuplien törmäyspiste
     * @param color Ammutun kuplan väri
     */
    public void lockToGrid(Point collisionPoint, int color){

        double distance;
        double shortestDistance = 99999;
        StaticBubble tempBubble = new StaticBubble(this,0,0,0,0,0,0,0,true);

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                StaticBubble temp = grid[i][j];
                if (temp.getType() == 0){
                    distance = Math.sqrt((collisionPoint.x-temp.getX())*(collisionPoint.x-temp.getX()) + (collisionPoint.y-temp.getY())*(collisionPoint.y-temp.getY()));
                    if (distance < shortestDistance){
                        shortestDistance = distance;
                        tempBubble = temp;
                    }
                }
            }
        }

        grid[tempBubble.getRow()][tempBubble.getColumn()] = new StaticBubble(this, 1, tempBubble.getRow(), tempBubble.getColumn(), color, tempBubble.getX(), tempBubble.getY(), width, tempBubble.isEven());
        if (!checkForGameOver()){
            ArrayList<StaticBubble> toRemove = checkForMatch(tempBubble.getRow(), tempBubble.getColumn());
            ArrayList<StaticBubble> toRemove2 = new ArrayList<StaticBubble>();
            ArrayList<StaticBubble> toRemove3 = new ArrayList<StaticBubble>();
            ArrayList<StaticBubble> toRemove4 = new ArrayList<StaticBubble>();
            toRemove2.clear();
            toRemove3.clear();
            toRemove4.clear();
            toRemove2.addAll(toRemove);

            boolean check = true;
            while (check){
                for (StaticBubble sb : toRemove2){
                    toRemove3 = checkForMatch(sb.getRow(), sb.getColumn());
                    for (StaticBubble sb2 : toRemove3) {
                        if (!toRemove.contains(sb2)){
                            toRemove.add(sb2);
                            toRemove4.add(sb2);
                        }
                    }
                }
                if (toRemove4.isEmpty()){
                    check = false;
                    break;
                }
                else {
                    toRemove2.clear();
                    for (StaticBubble sb : toRemove4) {
                        toRemove2.add(sb);
                    }
                    toRemove4.clear();
                    toRemove3.clear();
                }
            }

            if (toRemove.size() >= 3){
                for (StaticBubble sb : toRemove) {
                    int sleeptime = random.nextInt(50);
                    playSound(SOUND_POP);
                    grid[sb.getRow()][sb.getColumn()].setType(3);
                    try {
                        Thread.sleep(sleeptime);
                    } catch (InterruptedException e) {
                    }
                }
                checkForIsolated(toRemove.size());
            }

            if (!checkForWin()){
                if (shotCount % 5 == 0){
                    moveGrid();
                }
            }
            pickNextColor();
            allowShoot = true;
        }
    }

    /** Funktio, jossa tarkastetaan tietyn kuplan naapurit ruudukossa, ja palautetaan saman värin ketjut.
     * @param row Tutkittavan kuplan rivi
     * @param column Tutkittavan kuplan sarake
     * @return toRemove Lista naapurikuplista, jotka vastasivat väriltään tutkittavaa kuplaa
     */
    public ArrayList<StaticBubble> checkForMatch(int row, int column){

        int color = grid[row][column].getColor();
        ArrayList<StaticBubble> toRemove = new ArrayList<StaticBubble>();
        toRemove.clear();
        toRemove.add(grid[row][column]);

        if (grid[row][column].isEven()){ //EVEN
            if (column != 0 && grid[row][column-1].getColor() == color){
                toRemove.add(grid[row][column-1]);
            }
            if (column != columns-1 && grid[row][column+1].getColor() == color){
                toRemove.add(grid[row][column+1]);
            }
            if (row != 0 && grid[row-1][column].getColor() == color){
                toRemove.add(grid[row-1][column]);
            }
            if (row != 0 && grid[row-1][column+1].getColor() == color){
                toRemove.add(grid[row-1][column+1]);
            }
            if (row != rows-1 && grid[row+1][column].getColor() == color){
                toRemove.add(grid[row+1][column]);
            }
            if (row != rows-1 && grid[row+1][column+1].getColor() == color){
                toRemove.add(grid[row+1][column+1]);
            }
        }
        else { //ODD
            if (column != 0 && grid[row][column-1].getColor() == color){
                toRemove.add(grid[row][column-1]);
            }
            if (column != columns-1 && grid[row][column+1].getColor() == color){
                toRemove.add(grid[row][column+1]);
            }
            if (row != 0 && column != 0 && grid[row-1][column-1].getColor() == color){
                toRemove.add(grid[row-1][column-1]);
            }
            if (row != 0 && column != columns-1 && grid[row-1][column].getColor() == color){
                toRemove.add(grid[row-1][column]);
            }
            if (row != rows-1 && column != 0 && grid[row+1][column-1].getColor() == color){
                toRemove.add(grid[row+1][column-1]);
            }
            if (row != rows-1 && column != column-1 && grid[row+1][column].getColor() == color){
                toRemove.add(grid[row+1][column]);
            }
        }
        return toRemove;
    }

    /** Funktio, jossa tarkastetaan syntyikö ruudukkoon toisistaan erillisiä kuplaryhmiä, jotka pitää poistaa.
     * Funktio hyödyntää findGroup-funktiota.
     * @param scoreCount Väliaikainen pistetilanne, johon tarvittaessa lisätään
     */
    public void checkForIsolated(int scoreCount){

        ArrayList<StaticBubble> group = new ArrayList<StaticBubble>();
        boolean onTop = false;
        int maara = 0;
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                onTop = false;
                group.clear();
                if (grid[i][j].getType() == 1){
                    group.addAll(findGroup(i, j));
                    for (StaticBubble sb : group) {
                        if (sb.getRow() == 0){
                            onTop = true;
                        }
                    }
                    if (onTop == false){
                        scoreCount += group.size();
                        for (StaticBubble sb: group) {
                            int sleeptime = random.nextInt(50);
                            playSound(SOUND_POP);
                            grid[sb.getRow()][sb.getColumn()].setType(3);
                            try {
                                Thread.sleep(sleeptime);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                }
            }
        }
        addScore(scoreCount);
    }

    /** Funktio palauttaa tietyn kuplan "ryhmän", eli kaikki siihen ja toisiinsa liittyvät kuplat.
     * @param r Ensimmäisen kuplan rivi
     * @param c Ensimmäisen kuplan sarake
     * @return group Lista kuplista, jotka ovat kiinni alkuperäisessä kuplassa
     */
    public ArrayList<StaticBubble> findGroup(int r, int c){

        ArrayList<StaticBubble> group = new ArrayList<StaticBubble>();
        ArrayList<StaticBubble> group2 = new ArrayList<StaticBubble>();
        ArrayList<StaticBubble> group3 = new ArrayList<StaticBubble>();
        group.clear();
        group2.clear();
        group3.clear();
        group.add(grid[r][c]);
        group2.add(grid[r][c]);

        boolean check = true;
        while (check){
            for (StaticBubble sb : group2) {
                int row = sb.getRow();
                int column = sb.getColumn();
                if (grid[row][column].isEven()){ //EVEN
                    if (column != 0 && grid[row][column-1].getType() == 1){
                        if (!group.contains(grid[row][column-1])){
                            group.add(grid[row][column-1]);
                            group3.add(grid[row][column-1]);
                        }
                    }
                    if (column != columns-1 && grid[row][column+1].getType() == 1){
                        if (!group.contains(grid[row][column+1])){
                            group.add(grid[row][column+1]);
                            group3.add(grid[row][column+1]);
                        }
                    }
                    if (row != 0 && grid[row-1][column].getType() == 1){
                        if (!group.contains(grid[row-1][column])){
                            group.add(grid[row-1][column]);
                            group3.add(grid[row-1][column]);
                        }
                    }
                    if (row != 0 && grid[row-1][column+1].getType() == 1){
                        if (!group.contains(grid[row-1][column+1])){
                            group.add(grid[row-1][column+1]);
                            group3.add(grid[row-1][column+1]);
                        }
                    }
                    if (row != rows-1 && grid[row+1][column].getType() == 1){
                        if (!group.contains(grid[row+1][column])){
                            group.add(grid[row+1][column]);
                            group3.add(grid[row+1][column]);
                        }
                    }
                    if (row != rows-1 && grid[row+1][column+1].getType() == 1){
                        if (!group.contains(grid[row+1][column+1])){
                            group.add(grid[row+1][column+1]);
                            group3.add(grid[row+1][column+1]);
                        }
                    }
                }
                else { //ODD
                    if (column != 0 && grid[row][column-1].getType() == 1){
                        if (!group.contains(grid[row][column-1])){
                            group.add(grid[row][column-1]);
                            group3.add(grid[row][column-1]);
                        }
                    }
                    if (column != columns-1 && grid[row][column+1].getType() == 1){
                        if (!group.contains(grid[row][column+1])){
                            group.add(grid[row][column+1]);
                            group3.add(grid[row][column+1]);
                        }
                    }
                    if (row != 0 && column != 0 && grid[row-1][column-1].getType() == 1){
                        if (!group.contains(grid[row-1][column-1])){
                            group.add(grid[row-1][column-1]);
                            group3.add(grid[row-1][column-1]);
                        }
                    }
                    if (row != 0 && column != columns-1 && grid[row-1][column].getType() == 1){
                        if (!group.contains(grid[row-1][column])){
                            group.add(grid[row-1][column]);
                            group3.add(grid[row-1][column]);
                        }
                    }
                    if (row != rows-1 && column != 0 && grid[row+1][column-1].getType() == 1){
                        if (!group.contains(grid[row+1][column-1])){
                            group.add(grid[row+1][column-1]);
                            group3.add(grid[row+1][column-1]);
                        }
                    }
                    if (row != rows-1 && column != column-1 && grid[row+1][column].getType() == 1){
                        if (!group.contains(grid[row+1][column])){
                            group.add(grid[row+1][column]);
                            group3.add(grid[row+1][column]);
                        }
                    }
                }
            }
            if (!group3.isEmpty()){
                group2.clear();
                group2.addAll(group3);
                group3.clear();
            }
            else {
                check = false;
            }
        }
        return group;
    }

    /** Funktio, jossa lisätään kokonaispisteisiin laskettu summa poistettujen kuplien perusteella.
     * Mitä enemmän kuplia poistaa, sitä paremmat pisteet saa.
     * @param count Luku, montako kuplaa pelistä saatiin poistettua
     */
    public void addScore(int count){

        if (count < 4){
            score += (count * 5);
        }
        else if (count == 4 || count == 5 || count == 6){
            score += (count * 10);
        }
        else if (count == 7 || count == 8 || count == 9){
            score += (count * 15);
        }
        else if (count >= 10){
            score += (count * 20);
        }
    }

    /** Joka viidennellä ampumiskerralla pelimuodosta riippuen joko alennetaan pelin kattoa tai lisätään uusi kuplarivi. */
    public void moveGrid(){

        if (gameMode == 0){
            for (int i = 0; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    grid[i][j].setY(grid[i][j].getY() + width + (spacing / 2));
                }
            }
            gridY += width + (spacing / 2);
        }
        else if (gameMode == 1){

            boolean even = grid[0][0].isEven();
            StaticBubble[][] tempGrid = new StaticBubble[rows][columns];
            int bubbleX = 0;
            int bubbleY = 0 + spacing / 2;
            if (even == true){
                bubbleX = 0 + spacing;
                even = false;
            }
            else if (even == false){
                bubbleX = width / 2 + spacing;
                even = true;
            }

            for (int j = 0; j < columns; j++){
                int color = bubbleColors.get(random.nextInt(bubbleColors.size()));
                if (even == true && j == columns-1){
                    tempGrid[0][j] = new StaticBubble(this, 2, 0, j, 0, bubbleX, bubbleY, width, even);
                }
                else {
                    tempGrid[0][j] = new StaticBubble(this, 1, 0, j, color, bubbleX, bubbleY, width, even);
                }
                bubbleX += width + spacing;
            }

            for (int i = 1; i < rows; i++){
                for (int j = 0; j < columns; j++){

                    int color = grid[i-1][j].getColor();
                    bubbleX = grid[i-1][j].getX();
                    bubbleY = grid[i-1][j].getY() + width + (spacing / 2);
                    even = grid[i-1][j].isEven();

                    if (grid[i-1][j].getType() == 2){
                        tempGrid[i][j] = new StaticBubble(this, 2, i, j, 0, bubbleX, bubbleY, width, even);
                    }
                    else if (grid[i-1][j].getType() == 1){
                        tempGrid[i][j] = new StaticBubble(this, 1, i, j, color, bubbleX, bubbleY, width, even);
                    }
                    else if (grid[i-1][j].getType() == 0){
                        tempGrid[i][j] = new StaticBubble(this, 0, i, j, Color.GRAY, bubbleX, bubbleY, width, even);
                    }
                    else if (grid[i-1][j].getType() == 3){
                        tempGrid[i][j] = new StaticBubble(this, 3, i, j, color, bubbleX, bubbleY, width, even);
                    }
                }
            }
            grid = tempGrid;
        }
        gridMoves++;
        checkForGameOver();
    }

    /** Tarkastetaan, tuliko pelaajalle häviö, eli onko kuplia alle halutun rajan.
     * @return boolean True = häviö syntyi, False = häviötä ei syntynyt
     */
    public boolean checkForGameOver(){

        if (gameMode == 0){
            for (int i = rows-gridMoves-1; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    if (grid[i][j].getType() == 1){
                        allowShoot = false;
                        stopThread();
                        playSound(SOUND_LOSE);
                        showGameOverDialog(getResources().getString(R.string.lose));
                        return true;
                    }
                }
            }
        }
        else if (gameMode == 1){
            for (int i = rows-1; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    if (grid[i][j].getType() == 1){
                        allowShoot = false;
                        stopThread();
                        playSound(SOUND_LOSE);
                        showGameOverDialog(getResources().getString(R.string.gameover));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /** Tarkastetaan, tuliko pelaajalle voitto, eli onko ruudukossa kuplia jäljellä.
     * @return boolean True = voitto syntyi, False = voittoa ei syntynyt
     */
    public boolean checkForWin(){

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                if (grid[i][j].getType() == 1){
                    return false;
                }
            }
        }
        allowShoot = false;
        gameThread.setRunning(false);
        playSound(SOUND_VICTORY);
        showGameOverDialog(getResources().getString(R.string.win));
        return true;
    }

    /** Asetetaan ampujalle uusi (edellinen seuraava) kuplaväri. */
    public void pickCurrentColor(){
        currentColor = nextColor;
    }

    /** Asetetaan seuraavan ammuttavan kuplan väri. */
    public void pickNextColor(){

        ArrayList<Integer> tempColors = new ArrayList<>();
        tempColors.clear();
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                try {
                    if (grid[i][j].getType() == 1){
                        tempColors.add(grid[i][j].getColor());
                    }
                }
                catch (Exception e){

                }
            }
        }
        try {
            nextColor = tempColors.get(random.nextInt(tempColors.size()));
        }
        catch (Exception e){
            nextColor = bubbleColors.get(random.nextInt(bubbleColors.size()));
        }
        nextPaint.setColor(nextColor);
    }

    /** Piirretään pelin komponentit kankaalle.
     * @param canvas Kangas, johon peli piirretään.
     */
    public void drawToCanvas(Canvas canvas){

        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundPaint);
        canvas.drawLine(0, (int) (getScreenHeight() / 1.29095), getScreenWidth(), (int) (getScreenHeight() / 1.29095), darkPaint);
        canvas.drawLine(0, (int) (getScreenHeight() / 1.29095), getScreenWidth(), (int) (getScreenHeight() / 1.29095), linePaint);
        shooter.draw(canvas, darkPaint.getColor());

        currentBubble.setColor(currentColor);
        currentBubble.draw(canvas);

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                if (grid[i][j].getType() == 1 || grid[i][j].getType() == 3){
                    grid[i][j].draw(canvas);
                }
            }
        }
        if (shooter.getShotBubble() != null && shooter.getShotBubble().isOnScreen()){
            shooter.getShotBubble().draw(canvas);
        }

        canvas.drawText(getResources().getString(R.string.score), (int) (getScreenWidth() / 17), (int)(getScreenHeight() / 1.19), textPaint);
        canvas.drawText(String.valueOf(score), (int) (getScreenWidth() / 17), (int)(getScreenHeight() / 1.1), textPaint);

        if (difficulty == 0 ||  difficulty == 1){
            if (Locale.getDefault().getLanguage() == "en"){
                canvas.drawText(getResources().getString(R.string.next), (int) (getScreenWidth() / 1.32), (int)(getScreenHeight() / 1.19), textPaint);
            }
            else {
                canvas.drawText(getResources().getString(R.string.next), (int) (getScreenWidth() / 1.5), (int)(getScreenHeight() / 1.19), textPaint);
            }
            canvas.drawCircle((int)(getScreenWidth() / 1.25) + width/2, (int)(getScreenHeight() / 1.15) + width/2, (float) (width / 1.3), darkPaint);
            nextBubble.setColor(nextColor);
            nextBubble.draw(canvas);
        }
        if (difficulty == 0){
            shooter.drawHelper(canvas);
        }

        if (gridY > 0 &&  shotCount != 0){
            canvas.drawRect(0, 0, getScreenWidth(), gridY, buttonPaint);
            canvas.drawRect(0 + width/3, 0, getScreenWidth() - width/3, gridY - width/3, darkPaint);
        }

    }

    /** Päivitetään mahdollisen ammutun kuplan ja poistettavien kuplien paikat
     * @param time Kulunut aika.
     */
    private void updateBubblePositions(double time) {

        double interval = time / 1000.0;

        if (shooter.getShotBubble() != null) {
            shooter.getShotBubble().update(interval);
        }

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                if (grid[i][j].getType() == 3){
                    grid[i][j].update(interval);
                }
            }
        }
    }

    /** Funktio äänien toistamiseen.
     * @param soundId Ääniefektin tunniste
     */
    public void playSound(int soundId) {
        soundPool.play(soundMap.get(soundId), 1, 1, 1, 0, 1f);
    }

    /** Kosketuksen tunnistaminen. Jos näytöstä pidetään kiinni, tähdätään. Kun näytöstä päästetään irti, ammutaan.
     * @param e Kosketus
     */
    @Override
    public boolean onTouchEvent(MotionEvent e) {

        int action = e.getAction();
        if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN || action == MotionEvent.ACTION_MOVE) {
            alignShooter(e);
        }
        else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP){
            shootBubble(e);
        }
        return true;
    }

    /** Näytön koon muuttuessa päivitetään mitta-arvot.
     * @param width Uusi leveys
     * @param height Uusi korkeus
     * @param oldWidth Vanha leveys
     * @param oldHeight Vanha korkeus
     */
    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        screenWidth = width;
        screenHeight = height;
        textPaint.setTextSize((int) (TEXT_SIZE * screenHeight));
        textPaint.setAntiAlias(true);
    }

    /** Haetaan näytön leveys.
     * @return screenWidth Leveys
     */
    public int getScreenWidth() {
        return screenWidth;
    }

    /** Haetaan näytön korkeus.
     * @return screenHeight Korkeus
     */
    public int getScreenHeight() {
        return screenHeight;
    }

    /** Kun surfacen koko muuttuu.
     * @param holder
     * @param format
     * @param width
     * @param height
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    /** Kun surface luodaan.
     * @param holder
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!dialogIsDisplayed) {
            startGame();
            gameThread = new GameThread(holder);
            gameThread.setRunning(true);
            gameThread.start();
        }
    }

    /** Kun surface poistetaan.
     * @param holder
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        gameThread.setRunning(false);
        soundPool.release();
        soundPool = null;
        while (true) {
            try {
                gameThread.join();
                retry = false;
            } catch (Exception e) {
            }
        }
    }

    /** Sisäluokka peliä pyörittäville säikeille. */
    private class GameThread extends Thread {
        private SurfaceHolder surfaceHolder;
        private boolean threadIsRunning = true;

        /** Alustaja surfaceholderille
         * @param holder
         */
        public GameThread(SurfaceHolder holder) {
            surfaceHolder = holder;
            setName("GameThread");
        }

        /** Säikeen tila
         * @param running Onko säie käynnissä
         */
        public void setRunning(boolean running) {
            threadIsRunning = running;
        }

        /** Kutsuu pelin funktioita jatkuvasti pitääkseen sen yllä. */
        @Override
        public void run() {
            long previousFrameTime = System.currentTimeMillis();
            while (threadIsRunning) {
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        long currentTime = System.currentTimeMillis();
                        double elapsedTimeMS = currentTime - previousFrameTime;
                        updateBubblePositions(elapsedTimeMS);
                        bubbleCollision();
                        drawToCanvas(canvas);
                        previousFrameTime = currentTime;
                    }
                }
                finally {
                    if (canvas != null){
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    /** Apufunktio muille luokille, jotta ne voivat tarvittaessa lopettaa säikeen/pelin. */
    public void stopThread(){
        gameThread.setRunning(false);
    }

    /** Funktio pelin loppumisen dialogin näyttämiselle (sekä häviö että voitto).
     * @param messageId Teksti joko voitosta tai häviöstä
     */
    private void showGameOverDialog(final String messageId) {
        if (!dialogIsDisplayed){
            helpMessageId = messageId;
            helpScore = score;
            helpDialogIsDisplayed = dialogIsDisplayed;
            helpGameView = this;
            final DialogFragment gameResult = new MyAlertDialogFragment();

            activity.runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            dialogIsDisplayed = true;
                            gameResult.setCancelable(false);
                            gameResult.show(activity.getFragmentManager(), "gameover");
                        }
                    }
            );
        }
    }

    /** Sisäluokka pelin loppumisen dialogin näyttämiselle (sekä häviö että voitto). */
    public static class MyAlertDialogFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle bundle) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(helpMessageId);
            builder.setMessage(getResources().getString(R.string.score) + helpScore);
            builder.setPositiveButton(getResources().getString(R.string.reset), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    helpGameView.dialogIsDisplayed = false;
                    helpDialogIsDisplayed = false;
                    helpGameView.stopThread();
                    helpGameView.startGame();
                }});
            return builder.create();
        }
    }

}