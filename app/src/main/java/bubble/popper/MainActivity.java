package bubble.popper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;

/**
 * Luokka, jota kutsutaan ensimmäisenä sovellusta käynnistettäessä.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Luodaan sovelluksen ilmentymä ja alustetaan yläpalkki (Toolbar).
     * @param savedInstanceState Sovelluksen ilmentymä
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}