package bubble.popper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/**
 * Luokka vaikeustaso-asetuksen näyttämiseen ja sen muokkaamiseen.
 */
public class DifficultyDialogFragment extends DialogFragment {

    /** Luokan oma muuttuja vaikeustason tallentamiseen */
    private int position = 0;
    /** Kuuntelija valinnalle */
    DifficultyDialogFragment.SingleChoiceListener mListener;

    /** Dialogi, jonka käyttäjä näkee. Vaikeustasot haetaan Arrays-listasta.
     * @param  bundle */
    @Override
    public Dialog onCreateDialog(Bundle bundle) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] difficultys = getActivity().getResources().getStringArray(R.array.difficulty);

        builder.setTitle(getResources().getString(R.string.difficulty)).setSingleChoiceItems(difficultys, getGameFragment().getGameView().difficulty, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                position = i;
                System.out.println(position);
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.apply), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        getGameFragment().getGameView().difficulty = position;
                        getGameFragment().getGameView().stopThread();
                        getGameFragment().getGameView().startGame();
                    }
                }
        );

        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        return builder.create();
    }

    /** Kuuntelija vaikeustason valinnalle. Vain yksi vaikeustaso voi olla valittuna. */
    public interface SingleChoiceListener{
        void onPositiveButtonClicked(String[] difficultys, int position);
        void onNegativeButtonClicked();
    }

    /** Haetaan pelin fragmentti.
     * @return gameFragment Pelin fragmentti
     */
    private MainActivityFragment getGameFragment() {
        return (MainActivityFragment) getFragmentManager().findFragmentById(R.id.gameFragment);
    }

    /** Välitetään MainActivityFragmentille dialogin näyttö */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try {
            mListener = (DifficultyDialogFragment.SingleChoiceListener) context;
        }
        catch (Exception e) {
        }
    }

    /** Välitetään MainActivityFragmentille, ettei dialogia näytetä */
    @Override
    public void onDetach() {
        super.onDetach();
        MainActivityFragment fragment = getGameFragment();

        if (fragment != null){
            fragment.setDialogOnScreen(false);
        }
    }
}
