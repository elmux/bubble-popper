package bubble.popper;

import android.graphics.Color;
import java.util.Random;

/**
 * Kuplan aliluokka staattisille kuplille.
 */
public class StaticBubble extends Bubble{

    /** Kuplan tyyppi, käytetään sille onko kupla pelissä vai ei */
    private int type;
    /** Kuplan rivi ruudukossa */
    private int row;
    /** Kuplan sarake ruudukossa */
    private int column;
    /** Onko kuplan rivi parillinen vai pariton */
    private boolean even;
    /** Kuplan x-koordinaatti */
    private int x;
    /** Kuplan y-koordinaatti */
    private int y;
    /** Kuplan nopues x-akselilla */
    private float velocityX;
    /** Kuplan nopues y-akselilla */
    private float velocityY;

    /** Alustaja luokan muuttujille.
     * @param view  Fragmentin/pelin näkymä
     * @param type  Kuplan tyyppi. Käytetään siihen onko kupla pelissä vai ei
     * @param row   Kuplan rivi ruudukossa
     * @param column Kuplan sarake ruudukossa
     * @param color Kuplan väri
     * @param x     Kuplan x-koordinaatti
     * @param y     Kuplan y-koordinaatti
     * @param width Kuplan leveys
     * @param even  Onko kupla parillisella vai parittomalla rivillä
     */
    public StaticBubble(GameView view, int type, int row, int column, int color, int x, int y, int width, boolean even) {
        super(view, color, x, y, width);
        this.type = type;
        this.row = row;
        this.column = column;
        this.even = even;
        this.x = x;
        this.y = y;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        Random random = new Random();
        velocityX = -100 + random.nextInt(200);
        velocityY = -200;
    }

    /** Kun kupla poistetaan pelistä, luodaan tämä poistoanimaatio, jossa kupla tippuu.
     * @param interval Aikaväli
     */
    public void update(double interval) {
        collisionRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        colorRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        outlineRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        reflectionRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        x += (velocityX * interval);
        y += (velocityY * interval);
        if (velocityY < 0 || velocityY == 0){
            velocityY += 50;
        }
        else if (velocityY > 0){
            velocityY = (float) (velocityY * 1.3);
            velocityX = (float) (velocityX * 1.1);
        }
        if (collisionRect.top > view.getScreenHeight()){
            setType(0);
            setColor(Color.GRAY);
        }
    }

    /** Haetaan kuplan tyyppi.
     * @return type Kuplan tyyppi
     */
    public int getType(){
        return type;
    }

    /** Asetetaan kuplan tyyppi.
     * @param t Kuplan uusi tyyppi
     */
    public void setType(int t){
        type = t;
    }

    /** Haetaan kuplan rivi.
     * @return row Kuplan rivi
     */
    public int getRow() {
        return row;
    }

    /** Haetaan kuplan sarake.
     * @return column Kuplan sarake
     */
    public int getColumn() {
        return column;
    }

    /** Haetaan tieto, onko rivi parillinen vai pariton.
     * @return even True = parillinen, False = pariton
     */
    public boolean isEven(){
        return even;
    }
}
