package bubble.popper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

/**
 * Luokka pelissä käytettävälle kuulien ampujalle.
 */
public class Shooter {

    /** Pelin näkymä */
    private GameView view;
    /** Nuolen pituus */
    private int pointerLenght;
    /** Nuolen kärkien pituus */
    private int arrowLenght;
    /** Kulma, johon nuoli osoitetaan */
    private double angle;
    /** Kuplan säde */
    private int radius;
    /** Ammuttava kupla */
    private ShotBubble shotBubble;
    /** Nuolen päätepiste */
    private Point pointerEnd = new Point();
    /** Nuolen kärjen päätepiste */
    private Point arrow1 = new Point();
    /** Nuolen kärjen päätepiste */
    private Point arrow2 = new Point();
    /** Väri nuolelle */
    private Paint bPaint = new Paint();
    /** Teeman väri */
    private Paint colorPaint = new Paint();

    /** Alustaja luokan muuttujille.
     * @param view Pelin näkymä
     * @param pointerLenght Nuolen pituus
     * @param pointerWidth Nuolen paksuus
     * @param arrowLenght Nuolen kärjen pituus
     */
    public Shooter(GameView view, int pointerLenght, int pointerWidth, int arrowLenght) {
        this.view = view;
        this.pointerLenght = pointerLenght;
        this.arrowLenght = arrowLenght;
        bPaint.setStrokeWidth(pointerWidth);
        bPaint.setColor(Color.BLACK);
        align(Math.PI / 2);
        radius = (int) (view.getScreenHeight() * GameView.BUBBLE_RADIUS);
    }

    /** Suunnataan nuoli oikeaan asentoon ja lasketaan nuolen kärkien kulmat.
     * @param angle Nuolen kulma
     */
    public void align(double angle) {

        if (angle < 0.5235987756){
            angle = 0.5235987756;
        }
        else if (angle > 2.617993878){
            angle = 2.617993878 ;
        }
        this.angle = angle;
        double arrowangle = 3.1415926536 - (-0.7553981634 * angle);

        pointerEnd.x = (int) (-pointerLenght * Math.cos(angle)) + view.getScreenWidth() / 2;
        pointerEnd.y = (int) (view.getScreenHeight() - pointerLenght * Math.sin(angle));
        arrow1.x = (int) (view.getScreenWidth() / 2 - (pointerLenght - arrowLenght) * Math.cos(-3.1415926536 - arrowangle - 0.7853981634));
        arrow1.y = (int) (view.getScreenHeight() + (pointerLenght - arrowLenght) * Math.sin(-3.1415926536 - arrowangle - 0.7853981634 - 0.2)) ;
        arrow2.x = (int) (view.getScreenWidth() / 2 + (pointerLenght - arrowLenght) * Math.cos(arrowangle));
        arrow2.y = (int) (view.getScreenHeight() + (pointerLenght - arrowLenght) * Math.sin(arrowangle - 0.2));

    }

    /** Luodaan uusi ammuttava kupla.
     * @param width Ammuttavan kuplan leveys
     * @param color Ammuttavan kuplan väri
     */
    public void shoot(int width, int color) {

        int velocityX = (int) (GameView.BUBBLE_SPEED * view.getScreenWidth() * -Math.cos(angle));
        int velocityY = (int) (-GameView.BUBBLE_SPEED * view.getScreenWidth() * Math.sin(angle));

        shotBubble = new ShotBubble(view, color, view.getScreenWidth() / 2 - (width / 2), view.getScreenHeight() - (width / 2), width, velocityX, velocityY);
    }

    /** Piirretään ampuja (nuoli & pohja) pelin kankaalle. Kutsutaan GameView-luokasta.
     * @param canvas Kangas, johon peli piirretään.
     * @param currentColor Ammuttavan kuplan väri
     */
    public void draw(Canvas canvas, int currentColor) {

        colorPaint.setColor(currentColor);
        canvas.drawCircle(view.getScreenWidth() / 2, view.getScreenHeight() - view.width/2, (int) (radius * 2), colorPaint);
        canvas.drawLine(view.getScreenWidth() / 2, view.getScreenHeight() - radius, pointerEnd.x, pointerEnd.y, bPaint);
        canvas.drawLine(pointerEnd.x, pointerEnd.y, arrow1.x, arrow1.y, bPaint);
        canvas.drawLine(pointerEnd.x, pointerEnd.y, arrow2.x, arrow2.y, bPaint);
    }

    /** Piirretään ammuttavan kuplan suunta avustuksena, jos vaikeustaso on helpolla.
     * @param canvas Kangas, johon peli piirretään.
     */
    public void drawHelper(Canvas canvas){

        boolean empty = true;
        Point start = new Point(pointerEnd.x, pointerEnd.y);
        Point temp = new Point();
        double localangle = angle;
        int count = 0;
        while (empty){
            temp = start;
            if (localangle < 1.5707963268){
                start.x -= (int) ((radius) * Math.cos(localangle));
                start.y -= (int) ((radius) * Math.sin(localangle));
            }
            else if (localangle > 1.5707963268){
                start.x += (int) (-(radius) * Math.cos(localangle));
                start.y -= (int) ((radius) * Math.sin(localangle));
            }
            if (start.x < 0 || start.x > view.getScreenWidth()){
                start = temp;
                if (localangle < 1.5707963268){
                    localangle = 3.1415926536 - localangle;
                    start.x -= (int) ((radius) * Math.cos(localangle));
                    start.y -= (int) ((radius) * Math.sin(localangle));
                }
                else if (localangle > 1.5707963268){
                    localangle = 3.1415926536 - localangle;
                    start.x += (int) (-(radius) * Math.cos(localangle));
                    start.y -= (int) ((radius) * Math.sin(localangle));
                }
            }
            RectF tempRect = new RectF(start.x, start.y, start.x, start.y);
            if (view.helperCollision(tempRect) || start.y < 0){
                empty = false;
                break;
            }
            if (count == 1){
                canvas.drawCircle(start.x, start.y, radius / 2, bPaint);
                canvas.drawCircle(start.x, start.y, (float) (radius / 3), colorPaint);
                count++;
            }
            else if (count == 2){
                count = 0;
            }
            else {
                count++;
            }
        }
    }

    /** Haetaan ammuttu kupla, jotta siitä saadaan tietoa.
     * @return shotBubble Ammuttu kupla (luokka)
     */
    public ShotBubble getShotBubble() {
        return shotBubble;
    }

    /** Poistetaan kupla ruudulta. */
    public void removeShotBubble() {
        shotBubble = null;
    }

}
