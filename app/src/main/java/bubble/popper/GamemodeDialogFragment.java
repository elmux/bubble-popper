package bubble.popper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/**
 * Luokka pelimuoto-asetuksen näyttämiseen ja sen muokkaamiseen.
 */
public class GamemodeDialogFragment extends DialogFragment {

    /** Luokan oma muuttuja pelimuodon tallentamiseen */
    private int position = 0;
    /** Kuuntelija valinnalle */
    SingleChoiceListener mListener;

    /** Dialogi, jonka käyttäjä näkee. Pelimuodot haetaan Arrays-listasta.
     * @param  bundle */
    @Override
    public Dialog onCreateDialog(Bundle bundle) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] gamemodes = getActivity().getResources().getStringArray(R.array.game_mode);

        builder.setTitle(getResources().getString(R.string.gamemode)).setSingleChoiceItems(gamemodes, getGameFragment().getGameView().gameMode, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                position = i;
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.apply), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        getGameFragment().getGameView().gameMode = position;
                        getGameFragment().getGameView().stopThread();
                        getGameFragment().getGameView().startGame();
                    }
                }
        );

        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        return builder.create();
    }

    /** Kuuntelija pelimuodon valinnalle. Vain yksi pelimuoto voi olla valittuna. */
    public interface SingleChoiceListener{
        void onPositiveButtonClicked(String[] gamemodes, int position);
        void onNegativeButtonClicked();
    }

    /** Haetaan pelin fragmentti.
     * @return gameFragment Pelin fragmentti
     */
    private MainActivityFragment getGameFragment() {
        return (MainActivityFragment) getFragmentManager().findFragmentById(R.id.gameFragment);
    }

    /** Välitetään MainActivityFragmentille dialogin näyttö */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try {
            mListener = (SingleChoiceListener) context;
        }
        catch (Exception e) {
        }
    }

    /** Välitetään MainActivityFragmentille, ettei dialogia näytetä */
    @Override
    public void onDetach() {
        super.onDetach();
        MainActivityFragment fragment = getGameFragment();

        if (fragment != null)
            fragment.setDialogOnScreen(false);
    }
}
