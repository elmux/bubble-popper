package bubble.popper;

import android.graphics.RectF;

/**
 * Kuplan aliluokka ammutuille kuplille.
 */
public class ShotBubble extends Bubble{

    /** Kuplan nopues x-akselilla */
    private float velocityX;
    /** Kuplan nopues y-akselilla */
    private float velocityY;
    /** Totuusarvo sille, onko kupla ruudulla */
    private boolean onScreen;
    /** Kuplan x-koordinaatti */
    private int x;
    /** Kuplan y-koordinaatti */
    private int y;

    /** Alustaja luokan muuttujille.
     * @param view      Fragmentin/pelin näkymä
     * @param color     Kuplan väri
     * @param x         Kuplan x-koordinaatti
     * @param y         Kuplan y-koordinaatti
     * @param width     Kuplan leveys
     * @param velocityX Kuplan x-akselin nopeus
     * @param velocityY Kuplan y-akselin nopeus
     */
    public ShotBubble(GameView view, int color, int x, int y, int width, int velocityX, int velocityY) {
        super(view, color, x, y, width);
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        onScreen = true;
        this.x = x;
        this.y = y;
    }

    /** Tutkitaan osuiko ammuttu kupla staattiseen kuplaan.
     * @param bubble Staattinen kupla (StaticBubble), jota tutkitaan
     * @return boolean Palautetaan true tai false riippuen siitä, osuiko kupla
     */
    public boolean collides(StaticBubble bubble) {
        if (bubble.getType() == 1){
            return (RectF.intersects(collisionRect, bubble.outlineRect));
        }
        else {
            return false;
        }
    }

    /** Päivitetään kuplan paikka ruudulla. Jos kupla osuu ruudun reunaan, vaihdetaan sen suunta.
     * @param interval Aikaväli
     */
    public void update(double interval) {

        collisionRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        colorRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        outlineRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        reflectionRect.offset((int) (velocityX * interval), (int) (velocityY * interval));
        x += (velocityX * interval);
        y += (velocityY * interval);

        if (outlineRect.left < 0){
            velocityX *= -1;
            x = 0;
        }
        else if (outlineRect.right > view.getScreenWidth()){
            velocityX *= -1;
            x = view.getScreenWidth();
        }

    }

    /** Haetaan tieto, onko kupla ruudulla.
     * @return boolean Palautetaan true tai false riippuen siitä, onko kupla ruudulla
     */
    public boolean isOnScreen() {
        return onScreen;
    }

    /** Haetaan kuplan x-koordinaatti.
     * @return x Kuplan x-koordinaatti.
     */
    public int currentX(){
        return x;
    }

    /** Haetaan kuplan y-koordinaatti.
     * @return y Kuplan y-koordinaatti.
     */
    public int currentY(){
        return y;
    }
}
