package bubble.popper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Yliluokka kahdelle pelissä käytettäville kuplille (ShotBubble & StaticBubble).
 */
public class Bubble {

    /** Kuplan x-koordinaatti */
    private int x;
    /** Kuplan x-koordinaatti */
    private int y;
    /** Kuplan leveys */
    private int width;
    /** Törmäyksen tunnistamisen leveys */
    private int collisionwidth;
    /** Kuplan väri */
    private int color;
    /** Pelin näkymä */
    protected GameView view;
    /** Kuplan värimaali */
    protected Paint colorPaint = new Paint();
    /** Kuplan ääriviivamaali */
    protected Paint outlinePaint = new Paint();
    /** Kuplan heijastusmaali */
    protected Paint reflectionPaint = new Paint();
    /** Törmäysten tunnistamiseen */
    protected RectF collisionRect;
    /** Kuplan väriosuus */
    protected RectF colorRect;
    /** Kuplan ääriviiva */
    protected RectF outlineRect;
    /** Kuplan heijastus */
    protected RectF reflectionRect;

    /**
     * Luokan alustaja, jossa alustetaan arvot ja Paint-värit sekä kutsutaan setRects-funktiota.
     * @param view  Fragmentin/pelin näkymä
     * @param color Kuplan väri
     * @param x     Kuplan x-koordinaatti
     * @param y     Kuplan y-koordinaatti
     * @param width Kuplan leveys
     */
    public Bubble(GameView view, int color, int x, int y, int width) {
        this.view = view;
        this.x = x;
        this.y = y;
        this.width = width;
        this.color = color;
        colorPaint.setColor(color);
        outlinePaint.setColor(Color.BLACK);
        reflectionPaint.setColor(Color.WHITE);
        collisionwidth = width / 4;
        setRects();
    }

    /**  Kupla muodostuu neljästä komponentista. Tässä funktiossa asetetaan niiden koordinaatit.
     * collisionRect = Törmäysten tunnistamiseen.
     * colorRect = Kuplan väriosa.
     * outlineRect = Kuplan ääriviiva.
     * reflectionRect = Kuplan heijastus.
     */
    public void setRects(){
        collisionRect = new RectF(x + (collisionwidth), y + (collisionwidth), x+width - (collisionwidth), y+width - (collisionwidth));
        colorRect = new RectF(x, y, x + width, y + width);
        outlineRect = new RectF(x-10, y-10, x + width + 10, y + width + 10);
        reflectionRect = new RectF(x + (width / 20), (int) (y + (width / 1.3)), (int) (x + (width / 1.3)), y + (width / 20));
    }

    /**  Piirretään kuplan osat kankaaseen. Kuplat ovat luotu suorakulmioina (RectF), mutta ne piirretään ympyröinä (Oval). Kutsutaan GameView-luokasta.
     * @param canvas Kangas, johon peli piirretään.
     */
    public void draw(Canvas canvas) {
        canvas.drawOval(collisionRect, outlinePaint);
        canvas.drawOval(outlineRect, outlinePaint);
        canvas.drawOval(colorRect, colorPaint);
        canvas.drawOval(reflectionRect, reflectionPaint);
    }

    /** Haetaan kuplan väri toisesta luokasta.
     * @return color Kuplan väri
     */
    public int getColor(){
        return color;
    }

    /** Asetetaan kuplan väri uudeksi toisesta luokasta.
     * @param c Uusi väri
     */
    public void setColor(int c){
        color = c; colorPaint.setColor(color);
    }

    /** Haetaan kuplan x-koordinaatti toisesta luokasta.
     * @return x Kuplan x-koordinaatti
     */
    public int getX(){
        return x;
    }

    /** Haetaan kuplan y-koordinaatti toisesta luokasta.
     * @return y Kuplan y-koordinaatti
     */
    public int getY(){
        return y;
    }

    /** Asetetaan kuplan y-koordinaatti toisesta luokasta ja päivitetään komponentit.
     * @param newY Uusi y-koordinaatti
     */
    public void setY(int newY){
        y = newY;
        setRects();
    }
}
