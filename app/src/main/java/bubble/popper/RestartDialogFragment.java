package bubble.popper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/**
 * Luokka uudelleenkäynnistys-toiminnon näyttämiseen.
 */
public class RestartDialogFragment extends DialogFragment {

    /** Dialogi, jonka käyttäjä näkee.
     * @param  bundle */
    @Override
    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getResources().getString(R.string.restart));

        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        getGameFragment().getGameView().stopThread();
                        getGameFragment().getGameView().startGame();
                    }
                }
        );

        builder.setNegativeButton(getResources().getString(R.string.no), null);
        return builder.create();
    }

    /** Haetaan pelin fragmentti.
     * @return gameFragment Pelin fragmentti
     */
    private MainActivityFragment getGameFragment() {
        return (MainActivityFragment) getFragmentManager().findFragmentById(R.id.gameFragment);
    }

    /** Välitetään MainActivityFragmentille dialogin näyttö */
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        MainActivityFragment fragment = getGameFragment();

        if (fragment != null)
            fragment.setDialogOnScreen(true);
    }

    /** Välitetään MainActivityFragmentille, ettei dialogia näytetä */
    @Override
    public void onDetach() {
        super.onDetach();
        MainActivityFragment fragment = getGameFragment();

        if (fragment != null)
            fragment.setDialogOnScreen(false);
    }
}
