package bubble.popper;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Luokka, jossa luodaan näkymä pelille.
 */
public class MainActivityFragment extends Fragment {

    /** Pelin näkymä */
    private GameView gameView;
    /** Totuusarvo sille, onko dialogi näkyvillä */
    private boolean dialogOnScreen = false;

    /** Tyhjä alustaja. */
    public MainActivityFragment() {
    }

    /** Fragmentin näkymää luotaessa kutsuttava funktio.
     * @param inflater
     * @param container
     * @param savedInstanceState Ilmentymän tila
     * @return view Pelin näkymä
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(true);
        gameView = (GameView) view.findViewById(R.id.gameView);
        return view;
    }

    /** Sovelluksen menua/valikkoa luotaessa kutsuttava funktio.
     * @param menu Sovelluksen menu
     * @param inflater
     */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    /** Menun asetusta valittaessa kutsuttava funktio.
     * @param item Asetus, jota käyttäjä painoi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.gamemode:
                GamemodeDialogFragment gamemodeDialog = new GamemodeDialogFragment();
                gamemodeDialog.show(getFragmentManager(), "gamemode dialog");
                return true;
            case R.id.difficulty:
                DifficultyDialogFragment difficultyFragment = new DifficultyDialogFragment();
                difficultyFragment.show(getFragmentManager(), "difficulty dialog");
                return true;
            case R.id.restart:
                RestartDialogFragment restartDialog = new RestartDialogFragment();
                restartDialog.show(getFragmentManager(), "restart dialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Haetaan pelin näkymä.
     * @return gameView Pelin näkymä
     */
    public GameView getGameView() {
        return gameView;
    }

    /** Jos ruudulle luodaan dialogi, päivitetään luokan muuttuja.
     * @param visible Arvo sille onko dialogi esillä
     */
    public void setDialogOnScreen(boolean visible) {
        dialogOnScreen = visible;
    }
}